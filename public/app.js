var data = {
    "name":"shailesh",
    "class":"MCA",
    "phno":9981316323,
    "address1": {
        "city1":"Raipur",
        "state1":"CG"
    }
}

var jsonInput = document.getElementById('json-input')
var app = document.getElementById('app')
function onload(event) {
    jsonInput.value = JSON.stringify(data)
    printData('app','', data)
}
function showForm() {
    var newJSON = jsonInput.value
    data = isJsonString(newJSON) ? JSON.parse(newJSON) : data
    app.innerHTML = ""
    printData('app', '', data)
}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        alert('Not a valid json')
        return false;
    }
    return true;
}

function printData(divId, id, data) {
    var newId = id === '' ? '' : id+'.'
    for(var pt in data) {
        var div = document.getElementById(divId)
        var label = document.createElement('label')
        label.textContent = pt
        div.appendChild(label)
        if(typeof data[pt] == "object"){
            var select = document.createElement('select')
            select.id = newId+pt+'.opt'
            var obj_first = document.createElement('option')
            obj_first.value=""
            obj_first.innerText = "Select to add new"

            var obj = document.createElement('option')
            obj.value="obj"
            obj.innerText = "Object"
            var tn = document.createElement('option')
            tn.innerText = "Text/Number"
            tn.value = "tn"
            select.appendChild(obj_first)
            select.appendChild(obj)
            select.appendChild(tn)
            select.addEventListener('click', (e) => {
                addNewElement(e)
            })
            div.appendChild(select)
            var newDiv = document.createElement('div')
            newDiv.className="div-border"
            newDiv.id = pt
            div.appendChild(newDiv)
            console.log(pt+">>>>")
            printData(pt, newId+pt, data[pt])
        }
        else{
            var input = document.createElement('input')
            input.className = 'form-control'
            input.type = typeof data[pt] == 'number' ? 'number' : 'text'
            input.id = newId+pt
            input.value = data[pt]
            div.appendChild(input)
            console.log(pt, data[pt])
            input.addEventListener('change',(e) => {
                changeData(e.srcElement.id, e.srcElement.type, e.srcElement.value)
            })
        }
    }
}

function changeData(id, type, nData) {
    var val =  type == 'number' ? parseInt(nData) : nData
    console.log(typeof val, nData)
    var splits = id.split(/[.,\/ -]/);
    // console.log(splits);
    var newData = recChange(data, splits, nData)
    console.log('new data', newData)
    jsonInput.value = JSON.stringify(newData)
}

function recChange(data, array, value) {
    console.log(data, array)
    if(array.length > 1) {
        var nv = array.shift()
        data[nv] = recChange(data[nv], array, value)
    } else {
        data[array[0]] = value
        console.log(data)
        return data
    }
    console.log(data)
    return data
}

function addNewElement(e) {
    var opt = e.srcElement.value
    var id = e.srcElement.id
    var splits = id.split(/[.,\/ -]/);
    splits.splice(-1,1)
    if(opt === 'obj') {
        var objectName = prompt("Please enter your new object", "");
        if (objectName != null) {
            splits.push(objectName)
            var newData = recChange(data, splits, {})
            data = newData
            app.innerHTML = ""
            printData('app', '', data)
            jsonInput.value = JSON.stringify(newData)
        }

    } else if(opt === 'tn') {
        var valueName = prompt("Please enter your new value name", "");
        if (valueName != null) {
            splits.push(valueName)
            var newData = recChange(data, splits, "")
            data = newData
            app.innerHTML = ""
            printData('app', '', data)
            jsonInput.value = JSON.stringify(newData)
        }
    } else {

    }
}
